package pokemon;

/**
 *
 * @author Julio
 */
public abstract class TipoPokemon implements Capturar {

    private String nombre;
    private int ataque;
    private int defensa;
    private int salud;

    public TipoPokemon(String nombre, int ataque, int defensa, int salud) {
        this.nombre = nombre;
        this.ataque = ataque;
        this.defensa = defensa;
        this.salud = salud;
    }
    
    public int getDefensa() {
        return defensa;
    }

    public void setDefensa(int defensa) {
        this.defensa = defensa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getAtaque() {
        return ataque;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public int getSalud() {
        return salud;
    }

    public void setSalud(int salud) {
        this.salud = salud;
    }

    public abstract String fuerteContra();

    public abstract String debilContra();
}
