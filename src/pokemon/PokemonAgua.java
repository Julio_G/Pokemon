package pokemon;

/**
 *
 * @author Julio
 */
public class PokemonAgua extends TipoPokemon {

    private String tipoAgua;

    public PokemonAgua(String nombre, int ataque, int defensa, int salud, String tipoAgua) {
        super(nombre, ataque, defensa, salud);
        this.tipoAgua = tipoAgua;
    }

    public String getTipoAgua() {
        return tipoAgua;
    }

    public void setTipoAgua(String tipoAgua) {
        this.tipoAgua = tipoAgua;
    }

    @Override
    public String fuerteContra() {
        return "fuego";
    }

    @Override
    public String debilContra() {
        return "planta";
    }

    @Override
    public boolean capturar() {
        int numrand = (int) (Math.random() * (120 - 20) + 20);
        int resul = numrand - this.getSalud();
        boolean capturar = false;
        if (resul > this.getDefensa()) {
            capturar = true;
        }
        return capturar;
    }

}
