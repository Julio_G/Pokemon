package pokemon;

/**
 *
 * @author Julio
 */
public class PokemonFuego extends TipoPokemon {

    public PokemonFuego(String nombre, int ataque, int defensa, int salud) {
        super(nombre, ataque, defensa, salud);
    }

    @Override
    public String fuerteContra() {
        return "planta";
    }

    @Override
    public boolean capturar() {
        int numrand = (int) (Math.random() * (60 - 10) + 10);
        int resul = numrand + this.getSalud();
        boolean capturar = false;
        if (resul > (this.getDefensa() + this.getAtaque())) {
            capturar = true;
        }
        return capturar;
    }

    @Override
    public String debilContra() {
        return "agua";
    }

}
