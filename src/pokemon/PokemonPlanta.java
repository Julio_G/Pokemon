/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemon;

/**
 *
 * @author Julio
 */
public class PokemonPlanta extends TipoPokemon {

    private String habitat;

    public PokemonPlanta(String nombre, int ataque, int defensa, int salud, String habitat) {
        super(nombre, ataque, defensa, salud);
        this.habitat = habitat;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    @Override
    public String fuerteContra() {
        return "agua";
    }

    @Override
    public String debilContra() {
        return "fuego";
    }

    @Override
    public boolean capturar() {
        int numrand = (int) (Math.random() * (50 - 0) + 0);
        int resul = numrand + this.getAtaque();
        boolean capturar = false;
        if (resul > this.getSalud()) {
            capturar = true;
        }
        return capturar;
    }

}
