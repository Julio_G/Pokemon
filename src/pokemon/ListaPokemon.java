/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemon;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Julio
 */
public class ListaPokemon {

    HashMap<String, TipoPokemon> Pokemon = new HashMap<>();

    public ListaPokemon() {
        Pokemon = new HashMap<>();
    }

    public HashMap<String, TipoPokemon> getPokemon() {
        return Pokemon;
    }

    public void setPokemon(HashMap<String, TipoPokemon> Pokemon) {
        this.Pokemon = Pokemon;
    }

    public ArrayList<String> pokemonNombre() {
        ArrayList<String> pokeNombre = new ArrayList<>();
        for (String nombre : Pokemon.keySet()) {
            pokeNombre.add(nombre);
        }
        return pokeNombre;
    }

    public boolean existe(TipoPokemon p) {
        return Pokemon.containsKey(p.getNombre());
    }

    public void alta(TipoPokemon p) {
        Pokemon.put(p.getNombre(), p);
    }

    public TipoPokemon buscarPokemon(String nombre) {
        return Pokemon.get(nombre);
    }

    public ArrayList<TipoPokemon> bucarporTipo(String tipo) {
        ArrayList<TipoPokemon> todosTipo = new ArrayList<>();
        for (TipoPokemon p : Pokemon.values()) {
            if (tipo.equals(p.getClass().getSimpleName())) {
                todosTipo.add(p);
            }
        }
        System.out.println(Pokemon);
        System.out.println(todosTipo);
        return todosTipo;
    }

    public ArrayList<TipoPokemon> buscarTodos() {
        ArrayList<TipoPokemon> todos = new ArrayList<>();
        for (TipoPokemon p : Pokemon.values()) {
            todos.add(p);
        }
        return todos;
    }

    @Override
    public String toString() {
        return "ListaPokemon{" + "Pokemon=" + Pokemon + '}';
    }
}
